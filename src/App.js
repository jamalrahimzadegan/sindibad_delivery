import React, {Component} from 'react';
import {View, AsyncStorage} from 'react-native';
import Login from './components/main/Login';
import Main from './components/main/Main';
import Details from './components/main/Details';
import Profile from './components/main/Profile';
import Unpaid from './components/main/Unpaid';
import MyOrders from './components/main/MyOrders';
import RenderGoods from './components/flats/RenderGoods';
import TransactionsList from './components/main/TransactionsList';
import DrawerStyle from './components/DrawerStyle';
import {Provider} from 'react-redux';
import {store} from './core/reducers';
import Splash from './components/Splash';
import {createStackNavigator, createAppContainer, createDrawerNavigator} from 'react-navigation';

//---------First Navigator------------------------------------------------------------------------------------
const AppNavigator = createStackNavigator({
        Splash: {screen: Splash, navigationOptions: {header: null}},
        Login: {screen: Login, navigationOptions: {header: null}},
        Main: {screen: Main, navigationOptions: {header: null}},
        MyOrders: {screen: MyOrders, navigationOptions: {header: null}},
        Unpaid: {screen: Unpaid, navigationOptions: {header: null}},
        Details: {screen: Details, navigationOptions: {header: null}},
        RenderGoods: {screen: RenderGoods, navigationOptions: {header: null}},
        Profile: {screen: Profile, navigationOptions: {header: null}},
        TransactionsList: {screen: TransactionsList, navigationOptions: {header: null}},
    },
    {
        // initialRouteName: 'Unpaid'
    });

//---------Drawer Navigator------------------------------------------------------------------------------------
const Drawer = createDrawerNavigator({
    AppNavigator: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 275,
    drawerPosition: 'right',
    contentComponent: ({navigation}) => (<DrawerStyle navigation={navigation}/>),
});
//---------Drawer Navigator En------------------------------------------------------------------------------------
const DrawerEn = createDrawerNavigator({
    AppNavigator: {screen: AppNavigator, navigationOptions: {header: null}},
}, {
    drawerWidth: 270,
    drawerPosition: 'left',
    contentComponent: ({navigation}) => (<DrawerStyle navigation={navigation}/>),
});
const NavEn = createAppContainer(DrawerEn);
const Nav = createAppContainer(Drawer);

export default class App extends Component {
    constructor(props) {
        super(props);
        this.Lang = '';
        AsyncStorage.getItem('language').then((lan) => {
            if (lan) {
                this.Lang = lan;
                this.forceUpdate();
            }
        });
    }

    render() {
        if (this.Lang === 'En') {
            return (
                <Provider store={store}>
                    <NavEn/>
                </Provider>
            );
        } else {
            return (
                <Provider store={store}>
                    <Nav/>
                </Provider>
            );
        }
    }
}










