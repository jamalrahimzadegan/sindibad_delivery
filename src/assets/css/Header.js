import {StyleSheet, Dimensions, Platform} from 'react-native';
import {AppColorRed} from './main/Styles';


export default styles = StyleSheet.create({
    NavigationContainer:
        {
            backgroundColor: AppColorRed,
            height: 50,
            padding: 10,
            // paddingTop:10,
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '100%',
        },
    iosStatus:
        {
            paddingTop: 25,
            backgroundColor: 'white',
            width: '100%',
            display: Platform.OS === 'ios' ? 'flex' : 'none',
        },
    bottomNav: {
        borderTopLeftRadius: 150,
        borderTopRightRadius: 150,
        backgroundColor: '#fff',
        height: 17,
        width: '100%',
        alignSelf: 'center',
    },
    HeaderSep: {
        width: '100%',
        height: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },

});
