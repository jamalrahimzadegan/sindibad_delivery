import {Platform, StyleSheet} from 'react-native';
import {AppColorLightRed} from '../main/Styles';

export default styles = StyleSheet.create({

    MainContianer: {
        borderRadius: 4,
        margin: 5,
        padding: 8,
        flex: 1,
        width: '100%',
        alignSelf: 'center',
        backgroundColor: AppColorLightRed,
    },
    DetailsTexts: {
        fontFamily: 'kurdishFont',
        color: '#fff',
        fontSize: 13,

    },
    CatchGiveBtn: {
        width: 120,
        paddingVertical: 8,
        borderRadius: 4,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',

    },
});
