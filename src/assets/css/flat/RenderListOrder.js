import {Platform, StyleSheet} from 'react-native';
import {AppColorGray} from '../main/Styles';

export default styles = StyleSheet.create({

    RenderListOrderContianer: {
        flex: 1,
        width: '100%',
        marginVertical: 2,

    },
    MainTitles: {
        width: '70%',
    },
    MainOrdersIcons: {
        height: 80,
        width: '30%',
        resizeMode: 'contain',
    },
    ListBtnContainer: {
        ...Platform.select({
            ios: {
                shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                shadowOffset: {height: 0, width: 0},
            },
            android: {
                elevation: 10,
            },
        }),
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderRadius: 4,
        backgroundColor: AppColorGray,
        borderColor: '#444',
        marginVertical: 3,
        marginHorizontal: 10,
        padding: 10,
    },
    TextTitle: {
        fontFamily: 'kurdishFont',
        marginVertical: 4,
        color: '#fff',
        fontSize: 14,
    },
});
