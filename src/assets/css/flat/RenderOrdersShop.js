import {Platform, Dimensions, StyleSheet} from "react-native";

let H = Dimensions.get('window').height;
let W = Dimensions.get('window').width;
export default styles = StyleSheet.create({

    MainContianer: {
        flex: 1,
        width: '100%',
        alignSelf: 'center',
    },
    MarketButton: {
        borderRadius: 3,
        backgroundColor: '#ccc',
        padding: 10,
        marginTop: 15,
    },
    map: {
        marginVertical: 15,
        width:'100%',
        height:200,

    },
    ShopDetails: {
        backgroundColor: '#f0f0f0',
        marginHorizontal: 7,
        padding: 10
    },
    DetailsTexts: {
        fontFamily: 'kurdishFont',
        color: '#222',
        fontSize: 15,
        marginVertical: 5

    }
});
