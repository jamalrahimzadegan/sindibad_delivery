import {Dimensions, Platform, StyleSheet} from 'react-native';
import {AppColorRed} from './Styles';

let H = Dimensions.get('window').height;
let W = Dimensions.get('window').width;
export default styles = StyleSheet.create({

    DetailsContianer: {
        flex: 1,
        width: '100%',
        backgroundColor: AppColorRed,
    },
    ScrollDetails:
        {
            backgroundColor: '#fff',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            width: '100%',
            paddingHorizontal: 14,
            paddingBottom: 20,
        },
    map: {
        width: W,
        height: H / 3,

    },
    TitleDetailsText: {
        fontFamily: 'kurdishFont',
        color: '#333',
        fontSize: 16,
        marginVertical: 6
    },

    ConfirmButton: {
        borderRadius: 5,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 20,
        backgroundColor: AppColorRed,
        height: 45,
        width: '50%',
    },
});
