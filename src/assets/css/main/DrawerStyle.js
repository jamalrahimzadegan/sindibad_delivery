import {Platform, StyleSheet} from 'react-native';
import {AppColorRed} from './Styles';

export default styles = StyleSheet.create({
    DrawerContainer: {
        backgroundColor: AppColorRed,
        flex: 1,
        width: '100%',
    },
    Drawer: {
        flex: .8,
        width: '100%',
        paddingTop: 2,
        paddingBottom: 30,
        paddingHorizontal: 10,
        backgroundColor: AppColorRed,

    },
    DrawerSeperator: {
        width: '100%',
        height: 2,
        borderRadius: 10,
        backgroundColor: '#fff',
        opacity: .6,
        marginBottom: 10
    },
    text: {
        color: '#fff',
        fontSize: 15,
        fontFamily: 'kurdishFont',
    },
    Logo: {
        alignSelf: 'center',
        resizeMode: 'cover',
        height: 200,
        width: '100%',
        marginBottom: 10,
    },
    Details: {
        flexDirection: 'row',
        marginVertical: 3,
    },
    Seperator: {
        width: '100%',
        height: 2,
        opacity:.6,
        borderRadius:150,
        backgroundColor: '#fff',
        marginVertical: 15,
    },
    ButtonContainer: {
        alignSelf: 'flex-end',
        alignItems: 'flex-end',
        paddingVertical: 2,
    },
    EachButton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 7,

    },

});
