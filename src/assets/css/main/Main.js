import {Dimensions, Platform, StyleSheet} from 'react-native';
import {AppColorRed} from './Styles';

const H = Dimensions.get('window').height;
const W = Dimensions.get('window').width;

export default styles = StyleSheet.create({
    MainContianer: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',

    },
    SliderImg: {
        height: 200,
        width: W,
        resizeMode: 'cover',
        borderTopLeftRadius: 45 / 2,
        borderTopRightRadius: 45 / 2,
    },
    MainBtnContainer: {
        width:'99%',
        marginVertical: 7,
        marginHorizontal:7,
        alignSelf:'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    MainPageButtons: {
        backgroundColor: AppColorRed,
        justifyContent: 'center',
        alignItems: 'center',
        height: 45,
        borderRadius: 4,
        width: '32.5%',
        ...Platform.select({
            ios: {
                shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: 'red',
                shadowOffset: {height: 0, width: 0},
            },
            android: {
                elevation: 10,
            },
        }),
    },
    MainTextButton: {
        color: '#fff',
        fontSize: 14,
        fontFamily: 'kurdishFont',
    },
});
