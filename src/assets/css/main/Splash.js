import {StyleSheet, Dimensions} from 'react-native';
import {AppColorRed} from './Styles';

export default styles = StyleSheet.create({
    mainsp: {
        width: '100%',
        flex: 1,
        backgroundColor: '#fff',
    },
    imgf: {
        width: 120,
        height: 120,
        resizeMode: 'contain',
    },
    stltexte: {
        color: AppColorRed,
        fontFamily: 'kurdishFont',
        fontSize: 18,
        marginTop: 20,
    },
    langtext1: {
        color: 'gray',
        fontFamily: 'kurdishFont',
        fontSize: 16,
    },
    container: {
        width: '100%', height: Dimensions.get('window').height - 25, justifyContent: 'center',
        alignItems: 'center', backgroundColor: '#ffffff', resizeMode: 'contain',
    },
    SplashUpdateBtn: {
        width: '71%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: AppColorRed,
        marginTop: 12,
    },
    SplashUpdateTextBtn: {
        color: '#ffffff',
        fontSize: 15,
        fontFamily: 'kurdishFont',

    },
    SplashUpText: {
        color: '#ffffff',
        fontFamily: 'kurdishFont',

    },
    UpdateOuter: {
        width: '90%',
        borderRadius:8,
        paddingVertical: 20,
        // backgroundColor: '#47484b',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100,
    },
    languagesView: {
        width: '100%', height: 70, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center',
        marginTop: 50,
    },
    langbtns: {
        width: 70,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#666',
        borderWidth: .3,
        backgroundColor: AppColorRed,
        borderRadius: 4,
    },
    langtext: {color: '#fff', fontSize: 13},
});
