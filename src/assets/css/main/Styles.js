import {StyleSheet} from 'react-native';

export default Styles = StyleSheet.create({
    TTLErr:{
        color:'#fff',
        fontSize:16,
        fontFamily:'kurdishFont'
    }
});
export const AppColorRed='#d63031';
export const AppColorLightRed='#ff5252';
export const AppColorGray='#47484b';

export const PickerBg = [255, 118, 117, 1];
export const PickerToolBarBg = [214, 48, 49, 1];
export const PickerTitleColor = [255, 255, 255, 1];
export const PickerConfirmBtnColor = [255, 255, 255, 1];
export const PickerCancelBtnColor = [255, 255, 255, 1];
export const PickerFontColor = [0, 0, 0, 1];
