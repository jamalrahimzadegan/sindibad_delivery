import {StyleSheet, Dimensions, AsyncStorage} from 'react-native';
import {AppColorRed} from './Styles';


export default styles = StyleSheet.create({
    MainView: {
        flex: 1,
        width: '100%',
        height: Dimensions.get('window').height,
        backgroundColor: AppColorRed,
    },
    TransactionsFlatlist:
        {
            paddingHorizontal: 10,
            paddingBottom: 10,
            backgroundColor: '#fff',
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
        },
    eachFactorText: {
        textAlign: 'right',
        color: 'black',
        fontFamily: 'kurdishFont',
        fontSize: 15,
        paddingVertical: 3,
    },
    Container: {
        padding: 10,
    },
    Seperator: {
        alignSelf: 'center',
        width: '97%',
        backgroundColor: AppColorRed,
        height: 2,
        marginVertical: 5,
    },
});
