import React, {Component} from 'react';
import {
    Share,
    Alert,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    AsyncStorage, StatusBar,
} from 'react-native';
import styles from '../assets/css/main/DrawerStyle';
import {NavigationActions, StackActions} from 'react-navigation';
import URLS from '../core/URLS';
import {Connect} from '../core/Connect';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon6 from 'react-native-vector-icons/Entypo';
import {AppColorRed} from '../assets/css/main/Styles';
import Dic from '../core/Dic';
import {connect} from 'react-redux';
import RNRestart from 'react-native-restart'; // Import package from node modules

class DrawerStyle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ID: '',
            ProfilePhoto: '',
            Name: '',
            Family: '',
            Cash: '',
            Rank: '',
            MoarefCode: '',
        };
        this.Language = '';
    }


    //-------componentWillMount----------------------------------------------------------------------------------
    componentWillMount() {
        AsyncStorage.multiGet(['token', 'language', 'phone'])
            .then((x) => {
                this.Language = x[1][1];
                this.Phone = x[2][1];
                this.forceUpdate();
                this._Main();
            });
    }

    //-------componentDidUpdate----------------------------------------------------------------------------------
    componentDidUpdate(prevProps, prevState) {
        if (this.props.drawer_update != prevProps.drawer_update) {
            this._Main();
            AsyncStorage.multiGet([
                'language',
                'phone',
            ]).then((x) => {
                this.Language = x[0][1];
                this.Phone = x[1][1];
                this.forceUpdate()
            });
        }
    }

    render() {
        let Dict = Dic[this.props.Language  ? this.props.Language : 'Ku'];
        return (
            <ScrollView showsVerticalScrollIndicator={false}
                        style={[styles.DrawerContainer]}>
                <StatusBar backgroundColor={AppColorRed} barStyle="light-content"/>

                {/*--------Profile Photo------------------------------------------------------------------------------------*/}
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Profile')}>
                    {
                        this.state.ProfilePhoto ?
                            <Image
                                source={{uri: this.state.ProfilePhoto ? URLS.Media() + this.state.ProfilePhoto : null}}
                                style={[styles.Logo]}
                            /> :
                            <Image source={require('../assets/images/userDrawer.jpg')}
                                   style={[styles.Logo]}
                            />
                    }
                </TouchableOpacity>
                {/*--------Container------------------------------------------------------------------------------------*/}
                <View style={[styles.Drawer, {}]}>
                    <View style={[{
                        flexDirection: this.Language == 'En' ? 'row' : 'row-reverse',
                        alignSelf: this.Language == 'En' ? 'flex-start' : 'flex-end',
                    }, styles.Details]}>
                        <Text style={[styles.text, {
                            fontSize: 20,
                            textAlign: 'center',
                            alignSelf: 'center',
                        }]}>{this.state.Name + ' ' + this.state.Family}</Text>
                    </View>
                    {/*--Phone----------------------------------------*/}
                    <View style={[{
                        flexDirection: this.Language !== 'En' ? 'row' : 'row-reverse',
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                    }, styles.Details]}>
                        <Text style={[styles.text, {color:'#fff'}]}>{this.Phone}</Text>
                    </View>
                    {/*--Cash-------------------------------------------------------------------------------------*/}
                    <View style={[{
                        flexDirection: this.Language == 'En' ? 'row' : 'row-reverse',
                        alignSelf: this.Language == 'En' ? 'flex-start' : 'flex-end',
                    }, styles.Details]}>
                        <Text
                            style={[styles.text, {}]}>{Dict['cash']}: {this.state.Cash ? Connect.FormatNumber(this.state.Cash) : '-'}</Text>
                        {/*<Icon2 name={"coins"} color={AppColorRed} size={25} style={{marginHorizontal: 10}}/>*/}
                    </View>
                    {/*--Rank--------------------*/}
                    <View style={[{
                        flexDirection: this.Language == 'En' ? 'row' : 'row-reverse',
                        alignSelf: this.Language == 'En' ? 'flex-start' : 'flex-end',
                    }, styles.Details]} activeOpacity={.5}>
                        <Text
                            style={[styles.text, {}]}>{Dict['rank']} {this.state.Rank ? Connect.FormatNumber(this.state.Rank) : '-'}</Text>
                    </View>
                    {/*--Moaref--------------------*/}
                    <View style={[{
                        flexDirection: this.Language == 'En' ? 'row' : 'row-reverse',
                        alignSelf: this.Language == 'En' ? 'flex-start' : 'flex-end',
                    }, styles.Details]} activeOpacity={.5}>
                        <Text onPress={() => Share.share({message: this.state.MoarefCode})}
                              style={[styles.text, {color: AppColorRed}]}>
                            {Dict['moraef_code']}: {this.state.MoarefCode ? this.state.MoarefCode : '-'}</Text>
                        {/*<Icon name={"ios-git-branch"} color={AppColorRed} size={25} style={{marginHorizontal: 10}}/>*/}
                    </View>
                    <View style={styles.DrawerSeperator}/>
                    {/*--------Profile------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.Language !== 'En' ? 'row' : 'row-reverse',
                    }]}
                                      onPress={() => this.props.navigation.navigate('Profile')}
                                      activeOpacity={.5}>
                        <Text style={styles.text}>{Dict['edit_profile']}</Text>
                        {/*<Text style={styles.text}>{this.Profile}</Text>*/}
                        <View style={{alignItems: 'flex-end'}}>
                            <Icon2 name={'user'} color={'#fff'} size={30} style={{marginHorizontal: 10}}/>
                        </View>
                    </TouchableOpacity>
                    {/*--------Transactions------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.Language != 'En' ? 'row' : 'row-reverse',
                    }]}
                                      onPress={() => this.props.navigation.navigate('TransactionsList')}
                                      activeOpacity={.5}>
                        <Text style={styles.text}>{Dict['transactions']}</Text>
                        <View style={{alignItems: 'flex-end'}}>
                            <Icon3 name={'cash-usd'} color={'#fff'} size={30} style={{marginHorizontal: 10}}/>
                        </View>
                    </TouchableOpacity>
                    {/*--------Change Language------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.Language != 'En' ? 'row' : 'row-reverse',
                    }]} onPress={() => this._ChangeLang()}
                                      activeOpacity={.5}>
                        <Text style={styles.text}>{Dict['change_language']}</Text>
                        <View style={{alignItems: 'flex-end'}}>
                            <Icon6 name={'language'} color={'#fff'} size={30} style={{marginHorizontal: 10}}/>
                        </View>
                    </TouchableOpacity>
                    {/*--------logOut------------------------------------------------------------------------------------*/}
                    <TouchableOpacity style={[styles.EachButton, {
                        alignSelf: this.Language !== 'En' ? 'flex-end' : 'flex-start',
                        flexDirection: this.Language != 'En' ? 'row' : 'row-reverse',
                    }]} onPress={() => this._Exit(Dict)}>
                        <Text style={styles.text}>{Dict['t_profile_logout']}</Text>
                        <View style={{alignItems: 'flex-end'}}>
                            <Icon name={'md-exit'} color={'#fff'} size={30} style={{marginHorizontal: 10}}/>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }

    //-------Logout from the app in drawer------------------------------------------------
    _Exit(Dict) {
            Alert.alert('', Dict['logout_text'],
                [
                    {
                        text: Dict['t_main_cancel'],
                        onPress: () => null,
                        style: 'Cancel',
                    },
                    {
                        text: Dict['t_main_confirm'],
                        onPress: () => {
                            this.props.PURGE();
                            const resetAction = StackActions.reset({
                                index: 0,
                                key: null,
                                actions: [
                                    NavigationActions.navigate({
                                        routeName: 'Splash',
                                        params: {resetOrder: 1},
                                    }),
                                ],
                            });
                            this.props.navigation.dispatch(resetAction);
                        },
                    },
                ]);
    }


    //-------Refresh Drawer---------------------------------------------------------------------------
    _Main() {
        Connect.SendPRequest(URLS.Link() + 'getprofile', {userId: parseInt(this.props.id)})
            .then(res => {
                if (res) {
                    this.setState({
                            ProfilePhoto: res.image,
                            Name: res.fName,
                            Family: res.lName,
                            Cash: res.cash,
                            Rank: res.rank,
                            MoarefCode: res.moarefCode,
                        }, () => null,
                    );
                    AsyncStorage.setItem('Update', 'false');       //for drawer update
                    this.forceUpdate();
                }
            }).catch((e) => {
        });
    }

    //-------Navigatie to Home Screen (Main)---------------------------------------------------------------------------
    _ChangeLang() {
        AsyncStorage.multiRemove([
            'language',
            // 'align',
            // 'direction',
        ]).then(() => {
            const resetAction = StackActions.reset({
                index: 0,
                key: null,
                actions: [
                    NavigationActions.navigate({routeName: 'Splash', params: {resetOrder: 1}}),
                ],
            });
            this.props.navigation.dispatch(resetAction);
        });
    }

}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        drawer_update: state.drawer_update,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        PURGE: () => dispatch({type: 'PURGE'}),
        DrawerUpdateFn: () => dispatch({type: 'DrawerUpdateFn'}),


    };
}
export default connect(mapStateToProps, mapDispatchToProps)(DrawerStyle);
