import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity, FlatList, Button} from 'react-native';
import React from 'react';
import styles from '../../assets/css/flat/RenderGoods';
import {Connect} from '../../core/Connect';
import URLS from '../../core/URLS';
import {AppColorRed} from '../../assets/css/main/Styles';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';


class RenderGoods extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowDetails: false,
        };
    }


    render() {
        let item = this.props.item.item;
        let Dict = Dic[this.props.Language];

        return (
            <View style={[styles.MainContianer, {
                flexDirection: this.props.f_direction,
                alignItems: 'center',
                justifyContent: 'space-between',
            }]}>
                <Text
                    style={styles.DetailsTexts}>{this.props.Language === 'En' ? item.titleEn : this.props.Language === 'Fa' ? item.title : this.props.Language === 'Ar' ? item.titleAr : this.props.Language === 'Ku' ? item.titleKu : '-'}</Text>
                <TouchableOpacity
                    style={[styles.CatchGiveBtn, {backgroundColor: this._BtnColor(item)}]}
                    disabled={this.props.beginTime == null || item.giveCustomerTime !== null || item.cancelTime !== null}
                    onPress={() => {
                        this._BTN(item, Dict);
                    }}>
                    <Text
                        style={[styles.DetailsTexts, {color: '#fff'}]}>{this._BtnText(item, Dict)}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    //-----------item catch---------------------------------------------------------------------------------------------------------
    _DeliveryAcceptItem(id) {
        // this.Catched=false;
        // this.forceUpdate();
        Connect.SendPRequest(URLS.Link() + 'deliveryacceptitem', {id: parseInt(id)})
            .then(res => {
                console.warn('_ItemCatch: ' + res);
                if (res) {
                    this.props._GetDetails(this.props.orderId);
                    // this.setState({Orders: res});
                }
            });
    }

    //-----------GiveItem---------------------------------------------------------------------------------------------------------
    _Givecustomer(id, Dict) {
        // this.Catched=false;
        // this.forceUpdate();
        Connect.SendPRequest(URLS.Link() + 'givecustomer', {id: parseInt(id)})
            .then((res) => {
                // console.warn('_GiveItem: ' + res)
                if (res === true) {
                    this.props._GetDetails(this.props.orderId);
                } else {
                    this.dropdown.alertWithType('error', '', Dict['server_error']);

                }
            });
    }

    //-----------BTN---------------------------------------------------------------------------------------------------------
    _BTN(item, Dict) {
        if (item.cancelTime == null) {
            if (item.giveDeliveryTime > 0 && item.acceptDeliveryTime == null) {
                this._DeliveryAcceptItem(item.id);
            } else if (item.acceptDeliveryTime > 0 && item.giveCustomerTime == null) {
                this._Givecustomer(item.id, Dict);
            }
        } else {
            null; // do nothing
        }
    }

    //-----------BtnColor---------------------------------------------------------------------------------------------------------
    _BtnColor(item) {
        if (item.cancelTime == null) {
            if (item.giveDeliveryTime > 0 && item.acceptDeliveryTime == null) {
                return '#2ecc71';
            } else if (item.acceptDeliveryTime > 0 && item.giveCustomerTime == null) {
                return AppColorRed;
            } else if (item.giveCustomerTime !== null) {
                return '#999';
            }
        } else {
            return '#999';
        }
    }

    //-----------BtnText---------------------------------------------------------------------------------------------------------
    _BtnText(item, Dict) {
        if (item.cancelTime == null) {
            if (item.giveDeliveryTime > 0 && item.acceptDeliveryTime == null) {
                return Dict['good_take'];
            } else if (item.acceptDeliveryTime > 0 && item.giveCustomerTime == null) {
                return Dict['good_delivered'];
            } else if (item.giveCustomerTime !== null) {
                return Dict['deliver_done']; // do nothing
            }
        } else {
            return Dict['canceled']; // do nothing
        }
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(RenderGoods);