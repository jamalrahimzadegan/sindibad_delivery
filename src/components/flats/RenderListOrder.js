import {Component} from 'react';
import { View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/flat/RenderListOrder';
import FastImage from 'react-native-fast-image';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';

class RenderListOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let {item} = this.props.item;
        let Dict = Dic[this.props.Language];
        return (
            <View style={[styles.RenderListOrderContianer, {}]}>
                <TouchableOpacity style={[styles.ListBtnContainer, {flexDirection: this.props.f_direction}]}
                                  onPress={() => this.props.navigation.navigate('Details', {orderId: item.id})}>
                    <View style={[styles.MainTitles, {}]}>
                        <Text style={styles.TextTitle}>{Dict['customer']}: {item.customerName}</Text>
                        <Text style={styles.TextTitle}>{Dict['address']}: {item.address}</Text>
                        <Text style={styles.TextTitle}>{Dict['status']}: {item.status}</Text>
                    </View>
                    <FastImage source={require('./../../assets/images/delivery_cycle.png')}
                               style={styles.MainOrdersIcons}/>
                </TouchableOpacity>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        );
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(RenderListOrder);