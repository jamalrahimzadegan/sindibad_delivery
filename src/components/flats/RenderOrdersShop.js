import {Component} from 'react';
import {AsyncStorage, View, Text, TouchableOpacity, FlatList, Button, ScrollView} from 'react-native';
import React from 'react';
import styles from '../../assets/css/flat/RenderOrdersShop';
import MapView, {Marker} from 'react-native-maps';
import ListEmpty from '../../core/ListEmpty';
import RenderGoods from './RenderGoods';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

class RenderOrdersShop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ShowDetails: false,
            isMapReady: false,
        };
    }


    render() {
        let item = this.props.item.item.shop;
        let Dict = Dic[this.props.Language];

        return (
            <View style={styles.MainContianer}>
                <View onPress={() => this.setState({ShowDetails: !this.state.ShowDetails})}
                      style={styles.MarketButton}>
                    <Text
                        style={styles.DetailsTexts}>{this.props.Language === 'En' ? item.titleEn : this.props.Language === 'Fa' ? item.title : this.props.Language === 'Ar' ? item.titleAr : this.props.Language === 'Ku' ? item.titleKu : '-'}</Text>
                </View>
                <View style={[styles.ShopDetails, {}]}>
                  <Text
                        style={styles.DetailsTexts}>{Dict['mobile_num']}: {item.phoneNumber ? item.phoneNumber : '-'}</Text>
                    <Text
                        style={styles.DetailsTexts}>{Dict['phone_num']}: {item.fixedNumber ? item.fixedNumber : '-'}</Text>
                    <Text style={styles.DetailsTexts}>{Dict['address']}: {item.address ? item.address : '-'}</Text>
                    <Text
                        style={styles.DetailsTexts}>{Dict['description']}: {this.props.Language === 'En' ? item.descEn : this.props.Language === 'Fa' ? item.desc : this.props.Language === 'Ar' ? item.descAr : this.props.Language === 'Ku' ? item.descKu : '-'}</Text>

                    <FlatList
                        renderItem={(item, index) => <RenderGoods orderId={this.props.orderId}
                                                                  _GetDetails={this.props._GetDetails}
                                                                  beginTime={this.props.beginTime}
                                                                  item={item} navigation={this.props.navigation}/>}
                        showsVerticalScrollIndicator={false}
                        data={this.props.item.item.orderitems}
                        listKey={(item, index) => 'D' + index.toString()}
                        ListEmptyComponent={() => <ListEmpty
                            EmptyText={Dict['noitem']}
                            BgColor={'transparent'}/>}/>
                    {/*-------------Map---------------------------------------------------------------------------------*/}
                    <MapView
                        showsCompass={true}
                        followsUserLocation={true}
                        style={styles.map}
                        zoomControlEnabled={true}
                        region={
                            {
                                latitude: item.lat ? Number(item.lat) : 33.3152,
                                longitude: item.long ? Number(item.long) : 44.3661,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }
                        }>
                        <Marker
                            image={require('../../assets/images/Marker.png')}
                            coordinate={{
                                latitude: item.lat ? Number(item.lat) : 0,
                                longitude: item.long ? Number(item.long) : 0,
                            }}
                        />
                    </MapView>
                </View>
            </View>
        );
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(RenderOrdersShop);
