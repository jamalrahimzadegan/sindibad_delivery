import React from 'react';
import {
    FlatList,
    View,
    Text,
    AsyncStorage,
} from 'react-native';
import {Connect} from '../../core/Connect';
import styles from '../../assets/css/main/TransactionList';



export default class RenderTransactions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {        };
    }



    render() {
        let {item, Dict} =this.props;
        return (
            <View style={{flex: 1, alignItems: this.props.Language == 'En' ? 'flex-start' : 'flex-end'}}>
                <View style={[styles.Container, {}]}>
                    <View style={{flexDirection: this.props.f_direction}}>
                        <Text style={[styles.eachFactorText, {display: item.item.Refid ? 'flex' : 'none'}]}>
                            {Dict['tracking_number']} {item.item.Refid}</Text>
                    </View>
                    <View style={{flexDirection: this.props.f_direction}}>
                        <Text style={[styles.eachFactorText, {
                            display: item.item.AddCash ? 'flex' : 'none',
                        }]}>{Dict['increase']} {Connect.FormatNumber(parseInt(item.item.AddCash))}  </Text>
                    </View>
                    <View style={{flexDirection: this.props.f_direction}}>
                        <Text style={[styles.eachFactorText, {
                            display: item.item.DeCash ? 'flex' : 'none',
                        }]}>{Dict['decrease']} {Connect.FormatNumber(parseInt(item.item.DeCash))}  </Text>
                    </View>
                    <View style={{flexDirection: this.props.f_direction}}>
                        <Text style={styles.eachFactorText}>{Dict['fact_number']}</Text>
                        <Text style={styles.eachFactorText}>  {item.item.id}</Text>
                    </View>
                    <View style={{flexDirection: this.props.f_direction}}>
                        <Text
                            style={styles.eachFactorText}>{Dict['description']}: {this.props.Language == 'Fa' ? item.item.desc : this.props.Language == 'En' ? item.item.descEn : this.props.Language == 'Ar' ? item.item.descAr : this.props.Language == 'Ku' ? item.item.descKu : '-'}</Text>
                    </View>
                </View>
            </View>
        );
    }

}
