import {Component} from 'react';
import {AsyncStorage, ActivityIndicator, View, Text, ScrollView, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Details';
import Header from '../../core/Header';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import ListEmpty from '../../core/ListEmpty';
import {Connect} from '../../core/Connect';
import URLS from '../../core/URLS';
import RenderOrdersShop from '../flats/RenderOrdersShop';
import RenderGoods from '../flats/RenderGoods';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Loading: false,
            Empty: false,
            Details: [],
            Orders: [],
            Done: false,
        };
        this._GetDetails = this._GetDetails.bind(this);
    }

    componentWillMount() {
        this._GetDetails(this.props.navigation.getParam('orderId'));
    }

    render() {
        let {Details} = this.state;
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.DetailsContianer}>
                <Header navigation={this.props.navigation}/>
                <ScrollView showsVerticalScrollIndicator={false}
                            style={styles.ScrollDetails}>
                    <View style={{alignItems: this.props.Language === 'En' ? 'flex-start' : 'flex-end'}}>
                        <Text style={styles.TitleDetailsText}>{Dict['customer']}: {Details.customerName}</Text>
                        <Text style={styles.TitleDetailsText}>{Dict['status']}: {Details.status}</Text>
                        <Text style={styles.TitleDetailsText}>{Dict['mobile_num']}: {Details.tell}</Text>
                        <Text
                            style={styles.TitleDetailsText}>{Dict['price']}: {Details.price ? Connect.FormatNumber(parseInt(Details.price)) : '-'}</Text>
                        <Text style={styles.TitleDetailsText}>{Dict['address']}: {Details.address}</Text>

                    </View>
                    {/*---------OrderBtn------------------------------------------------------------------------------------------------------------------------*/}
                    <TouchableOpacity
                        onPress={() => this._OrderBtn(Details.id, Details.deliveryDoneTime, Details.confirmTime, Details.cancelTime, Details.beginTime, Dict)}
                        disabled={Details.deliveryDoneTime !== null || Details.cancelTime !== null}
                        style={[styles.ConfirmButton, {
                            backgroundColor: Details.deliveryDoneTime == null ? AppColorRed : '#ccc',
                        }]}>
                        {
                            this.state.Loading ?
                                <ActivityIndicator size={27} color={'#fff'}/>
                                :
                                <Text
                                    style={[styles.TitleDetailsText, {color: '#fff'}]}>{this._OrderBtnText(Details, Dict)}</Text>
                        }
                    </TouchableOpacity>
                    {/*-------------Map---------------------------------------------------------------------------------*/}
                    <MapView
                        showsCompass={true}
                        followsUserLocation={true}
                        style={styles.map}
                        zoomControlEnabled={true}
                        region={
                            {
                                latitude: Details.lat ? Number(Details.lat) : 33.3152,
                                longitude: Details.long ? Number(Details.long) : 44.3661,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }
                        }>
                        <Marker
                            image={require('../../assets/images/Marker.png')}
                            coordinate={{
                                latitude: Details.lat ? Number(Details.lat) : 0,
                                longitude: Details.long ? Number(Details.long) : 0,
                            }}
                        />
                    </MapView>


                    {/*-------SHOPS FLATLIST---------------------------------------------------------------------------------*/}
                    <FlatList
                        renderItem={(item, index) => <RenderOrdersShop
                            orderId={this.props.navigation.getParam('orderId')}
                            beginTime={Details.beginTime}
                            _GetDetails={this._GetDetails}
                            item={item} navigation={this.props.navigation}/>}
                        showsVerticalScrollIndicator={false}
                        data={this.state.Orders}
                        keyExtractor={(index) => index.toString()}
                        ListEmptyComponent={() => <ListEmpty
                            EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                            BgColor={'transparent'}/>}/>
                </ScrollView>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        );
    }

    //---------------------GetDetails-------------------------------------------------------------------------------------
    _GetDetails(id) {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'deliveryorderdetail', {id: parseInt(id)})
            .then(res => {
                console.log('Details delivery order: ');
                console.log(res);
                if (res) {
                    this.setState({Details: res.request, Orders: res.orders, Empty: false, Done: true});
                } else {
                    this.setState({Empty: false});
                }
            });
    }

    //---------------------_OrderBtn-------------------------------------------------------------------------------------
    _OrderBtn(id, deliveryDoneTime, confirmTime, cancelTime, beginTime, Dict) {
        // console.warn(id, deliveryDoneTime, confirmTime, cancelTime, beginTime)
        this.setState({Loading: true});
        if (confirmTime == null && cancelTime == null) {
            Connect.SendPRequest(URLS.Link() + 'confirmorder', {userId: parseInt(this.props.id), id: parseInt(id)})
                .then(res => {
                    // console.warn(res)
                    if (res == true) {
                        this._GetDetails(id);
                        this.setState({Loading: false});
                        this.dropdown.alertWithType('success', '', Dict['confirmed']);
                    } else {
                        this.setState({Loading: false});
                        this.dropdown.alertWithType('error', '', Dict['global_error']);
                    }
                });
        } else if (confirmTime > 0 && beginTime == null && cancelTime == null) {
            Connect.SendPRequest(URLS.Link() + 'beginorder', {id: parseInt(id)})
                .then(res => {
                    console.warn('beginorder: ' + res);
                    if (res == true) {
                        this._GetDetails(id);
                        this.setState({Loading: false});
                        this.dropdown.alertWithType('success', '', Dict['dispatched_con']);
                    } else {
                        this.setState({Loading: false});
                        this.dropdown.alertWithType('error', '', Dict['server_error']);
                    }
                });
        } else {
            if (this.state.Done == true && deliveryDoneTime == null && cancelTime == null) {
                Connect.SendPRequest(URLS.Link() + 'doneorder', {userId: parseInt(this.state.ID), id: parseInt(id)})
                    .then(res => {
                        console.log('deliverydone order: ');
                        console.warn('deliverydone: ' + res);
                        if (res) {
                            this._GetDetails(id);
                            this.setState({Loading: false});
                            this.dropdown.alertWithType('success', '', Dict['deliver_done']);
                        } else {
                            this.setState({Loading: false});
                            this.dropdown.alertWithType('error', '', Dict['server_error']);

                        }
                    });
            } else {
            }
        }
    }

    //---------------------_OrderBtn-------------------------------------------------------------------------------------
    _OrderBtnText(Details, Dict) {
        if (Details.cancelTime == null) {
            if (Details.confirmTime == null) {
                return Dict['t_main_confirm'];

            } else if (Details.confirmTime > 0 && Details.beginTime == null) {
                return Dict['dispatched_con'];
            } else {
                if (this.state.Done === true && Details.deliveryDoneTime == null) {
                    return Dict['deliver_finishing'];
                } else {
                    if (Details.deliveryDoneTime > 0) {
                        return Dict['deliver_done'];
                    }
                }
            }
        } else {
            return Dict['canceled'];
        }
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        f_direction: state.f_direction,
        text_align: state.text_align,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);
