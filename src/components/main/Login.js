import React from 'react';
import {
    ScrollView,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    Dimensions,

} from 'react-native';
import styles from '../../assets/css/main/Login';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {
    AppColorRed, PickerBg, PickerCancelBtnColor,
    PickerConfirmBtnColor,
    PickerFontColor, PickerTitleColor,
    PickerToolBarBg,
} from '../../assets/css/main/Styles';
import firebase from 'react-native-firebase';
import {Connect} from '../../core/Connect';
import CheckBox from 'react-native-check-box';
import URLS from '../../core/URLS';
import Picker from 'react-native-picker';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

const H = Dimensions.get('window').height;
const W = Dimensions.get('window').width;

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login1: 'flex',
            login2: 'none',
            btnok2: 'none',
            btnok: 'none',
            codeBack: '',
            userCode: '',
            phone: '',
            isUser: false,
            id: 0,
            fname: '',
            lname: '',
            moaref: '',
            login: '',
            Rules: '',
            ShowRules: false,
            IsChecked: false,
            City: '',
            Province: '',
            ProvinceList: [],
            ProvinceIdList: [],
            CityList: [],
            CityIdList: [],
            CityID: '',
            ProvinceID: '',

        };
        // this._handleBackButtonClick = this._handleBackButtonClick.bind(this);
        this._TextInp0 = null;
        this._TextInp1 = null;
        this._TextInp2 = null;
        this._TextInp3 = null;
        this.Language = '';
        //------------------------------------------------------------------------------------
        AsyncStorage.multiGet([
            'language',
        ]).then(x => {
            this.Language = x[0][1];
            this.forceUpdate();
        });
    }

    componentWillMount() {
        if (this.props.id) {
            this.props.navigation.replace('Main');
        }
        this._GetProvinceList();
        this._GetRoles();
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <ScrollView contentContainerStyle={{}} showsVerticalScrollIndicator={false}>
                <View style={styles.MainView}>
                    <StatusBar barStyle={'dark-content'} backgroundColor={'#ffffff'}/>
                    <View style={{display: this.state.login1, alignItems: 'center'}}>
                        <View style={[styles.MainView1]}>
                            <Icon1 name={'phone'} color={AppColorRed} size={25} style={{}}/>
                            <TextInput
                                value={this.state.phone}
                                onChangeText={(phone) => this.setState({phone})}
                                underlineColorAndroid={'transparent'}
                                keyboardType={'number-pad'}
                                returnKeyType={'done'}
                                placeholder={' 07 *********'}
                                style={styles.inp}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this._send(Dict)} style={styles.loginBtn}
                                          disabled={(this.state.btnok === 'none' ? false : true)}>
                            <ActivityIndicator color="#ffffff" style={{display: this.state.btnok}}/>
                            <Text
                                style={[styles.btntext, {display: this.state.btnok === 'none' ? 'flex' : 'none'}]}>{Dict['t_login_login']}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.MainView3, {display: this.state.login2}]}>
                        {/* ----------------------------------receive code------------------------------------ */}
                        <View style={styles.MainView2}>
                            <TextInput
                                value={this.state.userCode}
                                onChangeText={(userCode) => this.setState({userCode})}
                                underlineColorAndroid={'transparent'}
                                keyboardType={'number-pad'}
                                ref={(inp) => this._TextInp0 = inp}
                                returnKeyType={this.state.isUser ? 'done' : 'next'}
                                placeholder={'' + Dict['t_login_sms'] + ''}
                                style={styles.inp2}
                                blurOnSubmit={false}
                                onSubmitEditing={() => this.state.isUser ? this._TextInp0.blur() : this._TextInp1.focus()}
                            />
                            <Icon1 name={'textsms'} color={AppColorRed} size={25} style={{}}/>
                        </View>
                        {/* ---------------------------------------- first name--------------------------------------------*/}
                        <View style={[styles.MainView2, {display: this.state.isUser ? 'none' : 'flex'}]}>
                            <TextInput
                                value={this.state.fname}
                                onChangeText={(fname) => this.setState({fname})}
                                underlineColorAndroid={'transparent'}
                                returnKeyType={'next'}
                                ref={(inp) => this._TextInp1 = inp}
                                blurOnSubmit={false}
                                onSubmitEditing={() => this._TextInp2.focus()}
                                placeholder={Dict['t_profile_name']}
                                style={styles.inp2}
                            />
                            <Icon name={'user'} color={AppColorRed} size={25} style={{}}/>
                        </View>
                        {/* ---------------------------------------- last name--------------------------------------------*/}
                        <View style={[styles.MainView2, {display: this.state.isUser ? 'none' : 'flex'}]}>
                            <TextInput
                                value={this.state.lname}
                                onChangeText={(lname) => this.setState({lname})}
                                underlineColorAndroid={'transparent'}
                                returnKeyType={'next'}
                                blurOnSubmit={false}
                                onSubmitEditing={() => this._TextInp3.focus()}
                                ref={(inp) => this._TextInp2 = inp}
                                placeholder={Dict['t_profile_family']}
                                style={styles.inp2}
                            />
                            <Icon name={'user'} color={AppColorRed} size={25} style={{}}/>
                        </View>
                        {/*-----------Reagent---------------------------------------------------------------------------------*/}
                        <View style={[styles.MainView2, {display: this.state.isUser ? 'none' : 'flex'}]}>
                            <TextInput
                                value={this.state.moaref}
                                onChangeText={(moaref) => this.setState({moaref})}
                                underlineColorAndroid={'transparent'}
                                returnKeyType={'done'}
                                style={styles.inp2}
                                ref={(inp) => this._TextInp3 = inp}
                                placeholder={Dict['moraef_code']}
                            />
                            <Icon1 name={'verified-user'} color={AppColorRed} size={25} style={{marginLeft: 3}}/>
                        </View>
                        {/*-----------City and Province---------------------------------------------------------------------------------*/}
                        <View style={[styles.MainView2, {display: this.state.isUser ? 'none' : 'flex'}]}>
                            <TouchableOpacity onPress={() => this._SetProvince()}
                                              style={styles.PorCityBTN}>
                                <Text
                                    style={styles.cityproBtnText}>{this.state.Province !== '' ? this.state.Province : Dict['province']}</Text>
                                <Icon1 name={'location-city'} color={AppColorRed} size={25} style={{marginLeft: 3}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.MainView2, {display: this.state.isUser ? 'none' : 'flex'}]}>
                            <TouchableOpacity onPress={() => this._SetCity()}
                                              style={styles.PorCityBTN}>
                                <Text
                                    style={styles.cityproBtnText}>{this.state.City !== '' ? this.state.City : Dict['city_city']}</Text>
                                <Icon3 name={'home-city'} color={AppColorRed} size={25} style={{marginLeft: 3}}/>
                            </TouchableOpacity>
                        </View>
                        {/*----------------------------Confirm btn----------------------------------------------------------------*/}
                        <TouchableOpacity style={styles.stlbtnx}
                                          disabled={(this.state.btnok2 === 'none' ? false : true)}
                                          onPress={() => this._checkCode(Dict)}>
                            <ActivityIndicator color="#ffffff" style={{display: this.state.btnok2}}/>
                            <Text
                                style={[styles.btntextd, {display: this.state.btnok2 === 'flex' ? 'none' : 'flex'}]}>{Dict['t_main_confirm']}</Text>
                        </TouchableOpacity>
                    </View>

                    {/*-----------------Rules----------------------------------------------------------------------------------------------*/}
                    <View style={
                        [styles.Rules, {
                            top: this.state.ShowRules ? (this.state.login2 == 'flex' && !this.state.isUser ? H / 11 : 3000) : 3000,
                        }]
                    }>
                        <ScrollView style={{height: '85%', width: '95%'}}
                                    showsVerticalScrollIndicator={false}>
                            <Text style={[styles.btntext, {color: '#333', textAlign: 'right'}]}>
                                {this.state.Rules}
                            </Text>
                            <CheckBox onClick={() => this.setState({IsChecked: !this.state.IsChecked})}
                                      isIndeterminate={false}
                                      style={{}}
                                      isChecked={this.state.IsChecked}
                                      leftText={Dict['agree_rules']}
                                      rightText={Dict['agree_rules']}
                                      leftTextStyle={[styles.AcceptRulesText, {
                                          display: this.Language !== 'En' ? 'flex' : 'none',
                                      }]}
                                      rightTextStyle={[styles.AcceptRulesText, {
                                          display: this.Language === 'En' ? 'flex' : 'none',
                                      }]}
                                      checkedCheckBoxColor={'#333'}
                            />
                            <TouchableOpacity style={styles.ConfirmButton}
                                              onPress={() => this._ConfirmRules(Dict)}>
                                <Text style={[styles.btntext, {fontSize: 18}]}>{Dict['t_main_confirm']}</Text>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </View>
                {/*-------DropdownAlert---------------------------------------------------------------------------------------------------*/}
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: 'red'}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </ScrollView>

        );
    }

    //-------------------------checkCode-----------------------------------------------------------------------
    _checkCode(Dict) {
        console.warn('codeBack: ' + this.state.codeBack);
        // console.warn('userCode: '+this.state.userCode)
        if (this.state.userCode == this.state.codeBack) {
            if (this.state.isUser) {
                AsyncStorage.setItem('phone', this.state.phone);
                this.props.setId(parseInt(this.state.id));
                this._loginToken();
                this.setState({
                    login1: 'flex', login2: 'none', phone: '', fname: '', lname: '', userCode: '', ShowRules: false,
                });
                this.props.DrawerUpdateFn();
                this.props.navigation.replace('Main');
            } else {
                if (this.state.fname.length === 0) {
                    this.dropdown.alertWithType('error', '', Dict['login_noname']);
                } else if (this.state.lname.length === 0) {
                    this.dropdown.alertWithType('error', '', Dict['login_nofamily']);
                } else {
                    this.setState({btnok2: 'flex'});
                    Connect.SendPRequest(URLS.Link() + 'signup', {
                        username: this.state.phone,
                        role: 'delivery',
                        fName: this.state.fname,
                        rules: this.state.ShowRules,// for Accepting rules
                        acceptRole: 1,
                        cityId: this.state.CityID,
                        provinceId: this.state.ProvinceID,
                        lName: this.state.lname, ...this.state.moaref !== '' ? {moarefCode: this.state.moaref} : null,
                    }).then(res => {
                        this.setState({btnok2: 'none'});
                        // console.warn('signed up user', res);
                        if (res.hasOwnProperty('id') && res.hasOwnProperty('result') && res.result) {
                            this.props.setId(parseInt(res.id));
                            AsyncStorage.setItem('phone', this.state.phone);
                            this._loginToken2(res.id);
                            this.setState({
                                login1: 'flex',
                                login2: 'none',
                                phone: '',
                                fname: '',
                                lname: '',
                                userCode: '',
                            });
                            this.props.DrawerUpdateFn();
                            this.props.navigation.replace('Main');
                        } else {
                            this.dropdown.alertWithType('error', '', Dict['singup_serverError']);
                            this.setState({login1: 'flex', login2: 'none', ShowRules: true});
                        }
                    });
                }
            }
        } else {
            this.dropdown.alertWithType('error', '', Dict['t_login_wrongcode']);
        }
    }

    //-------------------------Send-----------------------------------------------------------------------
    _send(Dict) {
        // console.log({username: this.state.phone});
        if (/^07[\d]{9}$/.test(this.state.phone)) {
            this.setState({btnok: 'flex'});
            Connect.SendPRequest(URLS.Link() + 'addphone', {
                username: this.state.phone, role: 'delivery',
            }).then(res => {
                // console.warn('add Phone ', res);
                this.setState({btnok: 'flex'});
                if (res.hasOwnProperty('user') && res.user === false) {
                    this.setState({
                        codeBack: res.code,
                        login1: 'none',
                        login2: 'flex',
                        btnok: 'none',
                        ShowRules: true,
                    });
                } else if (res.hasOwnProperty('user') && res.user && res.hasOwnProperty('id')) {
                    this.props.setId(parseInt(res.id));
                    this.props.DrawerUpdateFn()
                    this.setState({
                        codeBack: res.code,
                        login1: 'none',
                        login2: 'flex',
                        isUser: true,
                        btnok: 'none',
                        id: res.id,
                        ShowRules: true,
                    });
                } else if (res.user && !res.access) {
                    this.setState({btnok: 'none'});
                    this.dropdown.alertWithType('error', '', Dict['singup_isUser']);
                } else {
                    this.dropdown.alertWithType('error', '', Dict['singup_globalerror']);
                    this.setState({btnok: 'none'});
                }
            });
        } else {
            this.dropdown.alertWithType('error', '', Dict['t_login_wrongnumber']);
        }
    }

    //-------------------------GetRoles -----------------------------------------------------------------------
    _GetRoles() {
        Connect.SendPRequest(URLS.Link() + 'rules', {})
            .then(res => {
                if (res) {
                    this.setState({Rules: res.value});
                }
            }).catch((e) => {
        });
    }

    //-------------------------ConfirmRules-----------------------------------------------------------------------
    _ConfirmRules(Dict) {
        this.state.IsChecked ? this.setState({ShowRules: false}) : this.dropdown.alertWithType('error', '', Dict['alert_rules']);
    }

    // -------------------------LoginTokens -----------------------------------------------------------------------
    _loginToken() {
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    // console.warn('Firebase Token ==', fcmToken);
                    this.props.dispatch(fcmRegister(fcmToken));
                    // this.setState({FcmToken: fcmToken});
                    Connect.SendPRequest(URLS.Link() + 'login', {userId: this.props.id, token: fcmToken});
                } else {
                    // user doesn't have a device token yet
                    // console.warn('User doesn\'t have a  token yet, Token = ', fcmToken);
                    firebase.messaging().requestPermission();
                    setTimeout(() => {
                        this._loginToken();
                    }, 3000);
                }
            });
    }

    _loginToken2(id) {
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    // console.warn('Firebase Token ==', fcmToken);
                    this.props.dispatch(fcmRegister(fcmToken));
                    // this.setState({FcmToken: fcmToken});
                    Connect.SendPRequest(URLS.Link() + 'login', {userId: id, token: fcmToken});
                } else {
                    // user doesn't have a device token yet
                    // console.warn('User doesn\'t have a  token yet, Token = ', fcmToken);
                    firebase.messaging().requestPermission();
                    setTimeout(() => {
                        this._loginToken2();
                    }, 3000);
                }
            });
    }

    //---------------------Get Province--------------------------------------------------------------------------------------
    _GetProvinceList() {
        Connect.SendPRequest(URLS.Link() + 'getprovince', {}).then(res => {
            // console.warn(res)
            AsyncStorage.getItem('language').then((lang) => {
                // console.warn('Lang: '+lang)
                let Ostan = [];
                let OstanID = [];
                for (let i = 0; i < res.length; i++) {
                    if (lang === 'En') {
                        Ostan.push(res[i].nameEn);
                        OstanID.push(res[i].id);
                    } else if (lang === 'Fa') {
                        Ostan.push(res[i].name);
                        OstanID.push(res[i].id);
                    } else if (lang === 'Ar') {
                        Ostan.push(res[i].nameAr);
                        OstanID.push(res[i].id);
                    } else if (lang === 'Ku') {
                        Ostan.push(res[i].nameKu);
                        OstanID.push(res[i].id);
                    }
                }
                this.setState({ProvinceList: Ostan, ProvinceIdList: OstanID});
            });
        }).catch((e) => {
            this.setState({Loading: false});
        });
    }

    //-------SelectProvince--------------------------------------------------------------------------------------------------
    _SetProvince(Dict) {
        Picker.init({
            pickerFontColor: PickerFontColor,
            pickerToolBarBg: PickerToolBarBg,
            pickerConfirmBtnColor: PickerConfirmBtnColor,
            pickerCancelBtnColor: PickerCancelBtnColor,
            pickerTitleColor: PickerTitleColor,
            pickerBg: PickerBg,
            pickerTextEllipsisLen: 20,
            pickerConfirmBtnText: Dict['t_main_confirm'],
            pickerCancelBtnText: Dict['t_main_cancel'],
            pickerTitleText: '',
            // pickerData: data,
            pickerData: this.state.ProvinceList.length !== 0 ? this.state.ProvinceList : [''],
            onPickerConfirm: data => {
                // console.warn('Ostan ID: '+this.state.ProvinceIdList[this.state.ProvinceList.indexOf(data.toString())])
                this.setState({
                    Province: data.toString(),
                    ProvinceID: this.state.ProvinceIdList[this.state.ProvinceList.indexOf(data.toString())],
                }, () => this._GetCityList(this.state.ProvinceIdList[this.state.ProvinceList.indexOf(data.toString())]));
            },
        });
        Picker.show();
    }

    //-------_GetCity--------------------------------------------------------------------------------------------------
    _GetCityList(ID) {
        Connect.SendGRequest(URLS.Link() + `getcity?id=${parseInt(ID)}`)
            .then(res => {
                // console.warn(res)
                AsyncStorage.getItem('language').then((lang) => {
                    // console.warn('Lang: ' + lang)
                    let City = [];
                    let CityID = [];
                    for (let i = 0; i < res.length; i++) {
                        if (lang === 'En') {
                            City.push(res[i].nameEn);
                            CityID.push(res[i].id);
                        } else if (lang === 'Fa') {
                            City.push(res[i].name);
                            CityID.push(res[i].id);
                        } else if (lang === 'Ar') {
                            City.push(res[i].nameAr);
                            CityID.push(res[i].id);
                        } else if (lang === 'Ku') {
                            City.push(res[i].nameKu);
                            CityID.push(res[i].id);
                        } else {
                            null;
                        }
                    }
                    this.setState({Loading: false, CityList: City, CityIdList: CityID});
                });
            }).catch((e) => {
            this.setState({Loading: false});
        });
    }

    //-------SelectCity--------------------------------------------------------------------------------------------------
    _SetCity(Dict) {
        Picker.init({
            pickerFontColor: PickerFontColor,
            pickerToolBarBg: PickerToolBarBg,
            pickerConfirmBtnColor: PickerConfirmBtnColor,
            pickerCancelBtnColor: PickerCancelBtnColor,
            pickerTitleColor: PickerTitleColor,
            pickerBg: PickerBg,
            pickerTextEllipsisLen: 20,
            pickerConfirmBtnText: Dict['t_main_confirm'],
            pickerCancelBtnText: Dict['t_main_cancel'],
            pickerTitleText: '',
            // pickerData: data,
            pickerData: this.state.CityList.length !== 0 ? this.state.CityList : [''],
            onPickerConfirm: data => {
                // console.warn('city:'+ data.toString());
                this.setState({
                    City: data.toString(),
                    CityID: this.state.CityIdList[this.state.CityList.indexOf(data.toString())],
                }, () => null);
            },
        });
        Picker.show();
    }
}

function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        drawer_update: state.drawer_update,

    };
}

function mapDispatchToProps(dispatch) {
    return {
        setId: (x) => dispatch({type: 'setId', payload: {id: x}}),
        DrawerUpdateFn: () => dispatch({type: 'DrawerUpdateFn'}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
