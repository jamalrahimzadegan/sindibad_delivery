import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Main';
import Header from '../../core/Header';
import Carousel from 'react-native-snap-carousel';
import URLS from '../../core/URLS';
import FastImage from 'react-native-fast-image';
import ListEmpty from '../../core/ListEmpty';
import RenderListOrder from '../flats/RenderListOrder';
import {Connect} from '../../core/Connect';
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

const H = Dimensions.get('window').height;
const W = Dimensions.get('window').width;

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Empty: false,
            HasDoing: null,
            entries: [],
            Orders: [],
            MyYrders: [],
        };
    }

    componentDidMount() {
        this._GetSliders();
        this.props.navigation.addListener('willFocus', payload => {
            this._GetNewOrders();
            this._GetDoing();
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <View style={{
                }}>
                    <View style={{backgroundColor: AppColorRed}}>
                        <Carousel
                            data={this.state.entries}
                            renderItem={this._renderItem}
                            autoplay={true}
                            ref={(c) => this._carousel = c}
                            sliderWidth={W}
                            itemWidth={W}/>
                    </View>
                    <View style={styles.MainBtnContainer}>
                        <TouchableOpacity style={styles.MainPageButtons} onPress={() => this._Doing(Dict)}>
                            <Text style={styles.MainTextButton}>{Dict['doing_button']}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.MainPageButtons}
                                          onPress={() => this.props.navigation.navigate('Unpaid')}>
                            <Text style={styles.MainTextButton}>{Dict['not_sattled']}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.MainPageButtons}
                                          onPress={() => this.props.navigation.navigate('MyOrders')}>
                            <Text style={[styles.MainTextButton,{}]}>{Dict['my_orders']}</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <FlatList
                    renderItem={(item, index) => <RenderListOrder item={item} navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Orders}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: 'red'}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        );
    }

    //---Slider Render--------------------------------------------------------------------------------------------------
    _renderItem({item, index}) {
        return (
            <FastImage source={{uri: URLS.Slider() + item}}
                       style={styles.SliderImg}/>
        );
    }

    //---------------------_GetSliders--------------------------------------------------------------------------------------
    _GetSliders() {
        Connect.SendPRequest(URLS.Link() + 'slider', {})
            .then(res => {
                // console.warn(res)
                if (res) {
                    this.setState({entries: res});
                }
            });
    }

    //---------------------Get Doing-------------------------------------------------------------------------------------
    _GetDoing() {
        Connect.SendPRequest(URLS.Link() + 'deliverydoing', {userId: parseInt(this.props.id)})
            .then(res => {
                console.log('GetDoing');
                console.log(res.id);
                if (res) {
                    this.setState({HasDoing: res.id});
                } else {
                    this.setState({HasDoing: null});
                }
            });
    }

    //---------------------HasDoing-------------------------------------------------------------------------------------
    _Doing(Dict) {
        if (this.state.HasDoing !== null) {
            this.props.navigation.navigate('Details', {orderId: parseInt(this.state.HasDoing)});
        } else {
            this.dropdown.alertWithType('error', '', Dict['doing_message']);
        }
    }

    //---------------------GetNewOrders-------------------------------------------------------------------------------------
    _GetNewOrders() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'deliverynew', {userId: parseInt(this.props.id)})
            .then(res => {
                // console.log('deliverynew:');
                // console.log(res);
                if (res) {
                    this.setState({Orders: res, Empty: false});
                } else {
                    this.setState({Empty: false});

                }
            });
    }
}

//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        drawer_update: state.drawer_update,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
