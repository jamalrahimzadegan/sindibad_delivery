import {Component} from 'react';
import { View,  FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/MyOrders';
import Header from '../../core/Header';
import URLS from "../../core/URLS";
import ListEmpty from "../../core/ListEmpty";
import RenderListOrder from "../flats/RenderListOrder";
import {Connect} from "../../core/Connect";
import {connect} from 'react-redux';
import Dic from '../../core/Dic';

 class MyOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entries: [],
            MyOrders: [],
            Empty: false
        };
    }

    componentDidMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetMine()
        });
    }

    render() {
        let Dict = Dic[this.props.Language];

        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    style={styles.MyOrdersFlatlist}
                    renderItem={(item, index) => <RenderListOrder item={item} navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.MyOrders}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }

    //---------------------Get my orders-------------------------------------------------------------------------------------
    _GetMine() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + "deliverymyorders", {userId: parseInt(this.props.id)})
            .then(res => {
                // console.log('My orders:')
                // console.log(res)
                if (res) {
                    this.setState({MyOrders: res, Empty: false});
                } else {
                    this.setState({Empty: false});
                }
            });
    }

}
//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
    };
}
function mapDispatchToProps(dispatch) {
    return {};
}
export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);