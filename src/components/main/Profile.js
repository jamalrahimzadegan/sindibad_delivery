import React from 'react';
import {
    ActivityIndicator,
    View,
    TouchableOpacity,
    Text, Image, Dimensions, AsyncStorage, ScrollView, TextInput, Alert,
} from 'react-native';
import styles from '../../assets/css/main/Profile';
import DropdownAlert from 'react-native-dropdownalert';
import Styles, {
    AppColorRed,
    PickerBg,
    PickerCancelBtnColor,
    PickerConfirmBtnColor,
    PickerFontColor, PickerTitleColor,
    PickerToolBarBg,
} from '../../assets/css/main/Styles';
import RNFetchBlob from 'react-native-fetch-blob';
import ImagePicker from 'react-native-image-picker';
import FastImage from 'react-native-fast-image';
import URLS from '../../core/URLS';
import {Connect} from '../../core/Connect';
import Picker from 'react-native-picker';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';
import Header from '../../core/Header';


class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            CM: false,
            Rank: '',
            Token: '',
            Name: '',
            Family: '',
            Phone: '',
            CodeMelli: '',
            FixedNumber: '',
            CompanyName: '',
            Desc: '',
            ProfilePhoto: '',
            NewImageData: '',
            NewImageName: '',
            UploadingPhoto: false,
            Loading: false,
            CompanyAddress: '',
            Lat: '',
            Long: '',
            City: '',
            Province: '',
            ProvinceList: [],
            ProvinceIdList: [],
            CityList: [],
            CityIdList: [],
            CityID: '',
            ProvinceID: '',
        };
    }

    componentWillMount() {
        this._GetDetails(this.props.id);
        this._GetProvince();
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={{flex: 1, backgroundColor: AppColorRed}}>
                <Header navigation={this.props.navigation}/>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={styles.MainView}>
                    <View style={styles.Container}>
                        <TouchableOpacity
                            style={[{display: this.state.ProfilePhoto ? 'flex' : 'none', height: 180, width: '40%'}]}
                            onPress={() => this._PhotoUpdate(Dict)}>
                            {
                                this.state.ProfilePhoto ?
                                    <FastImage
                                        source={{uri: this.state.ProfilePhoto ? URLS.Media() + this.state.ProfilePhoto : null}}
                                        style={[styles.ProfilePhoto, {marginTop: 8, resizeMode: 'cover'}]}
                                    /> :
                                    <FastImage source={require('../../assets/images/user.png')}
                                               style={[styles.ProfilePhoto, {}]}
                                    />
                            }
                        </TouchableOpacity>
                        <ActivityIndicator size={50} style={[styles.PhotoUploadLoading, {
                            left: this.state.UploadingPhoto ? 50 : -2000,
                        }]} color={AppColorRed}/>
                        <View style={{flexShrink: 1, alignSlef:'flex-start', width:'58%'}}>
                            <Text style={styles.NameText}>{this.state.Name} {this.state.Family}</Text>
                            <Text style={styles.Label} >{this.state.Phone}</Text>
                        </View>
                    </View>
                    <View style={styles.seprator}/>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_name']}</Text>
                        <TextInput
                            value={this.state.Name}
                            onChangeText={(name) => this.setState({Name: name})}
                            underlineColorAndroid={'transparent'}
                            // keyboardType={"number-pad"}
                            // returnKeyType={"done"}
                            placeholder={this.state.Name ? this.state.Name : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_family']}</Text>
                        <TextInput
                            value={this.state.Family}
                            onChangeText={(name) => this.setState({Family: name})}
                            underlineColorAndroid={'transparent'}
                            // keyboardType={"number-pad"}
                            // returnKeyType={"done"}
                            placeholder={this.state.Family ? this.state.Family : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_codemeli']}</Text>

                        <TextInput
                            editable={this.state.CM ? false : true}
                            value={this.state.CodeMelli}
                            onChangeText={(name) => this.setState({CodeMelli: name})}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            placeholder={this.state.CodeMelli ? this.state.CodeMelli : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['phone_num']}</Text>
                        <TextInput
                            value={this.state.FixedNumber}
                            onChangeText={(name) => this.setState({FixedNumber: name})}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'number-pad'}
                            returnKeyType={'done'}
                            placeholder={this.state.FixedNumber ? this.state.FixedNumber : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['t_profile_company']}</Text>
                        <TextInput
                            value={this.state.CompanyName}
                            onChangeText={(name) => this.setState({CompanyName: name})}
                            underlineColorAndroid={'transparent'}
                            placeholder={this.state.CompanyName ? this.state.CompanyName : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['description']}</Text>
                        <TextInput
                            value={this.state.Desc}
                            onChangeText={(name) => this.setState({Desc: name})}
                            underlineColorAndroid={'transparent'}
                            multiline={true}
                            placeholder={this.state.Desc ? this.state.Desc : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['mobile_num']}</Text>
                        <TextInput
                            value={this.state.CompanyAddress}
                            onChangeText={(name) => this.setState({CompanyAddress: name})}
                            underlineColorAndroid={'transparent'}
                            placeholder={this.state.CompanyAddress ? this.state.CompanyAddress : ''}
                            style={[styles.Input, {textAlign: this.props.text_align}]}
                        />
                    </View>

                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={[styles.Label, {}]}>{Dict['province']}</Text>
                        <TouchableOpacity onPress={() => this._SelectProvince(Dict)}
                                          style={[styles.Input, {}]}>
                            <Text style={[styles.Label, {
                                paddingVertical: 10,
                                color: '#fff',
                                textAlign: this.props.text_align,
                            }]}>{this.state.Province}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.EachLine, {flexDirection: this.props.f_direction}]}>
                        <Text style={styles.Label}>{Dict['city_city']}</Text>
                        <TouchableOpacity onPress={() => this._SelectCity(Dict)}
                                          style={[styles.Input, {}]}>
                            <Text style={[styles.Label, {
                                paddingVertical: 10,
                                color: '#fff',
                                textAlign: this.props.text_align,
                            }]}>{this.state.City}</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={styles.UpdateButton} onPress={() => this._UpdateProfile(Dict)}>
                        {
                            this.state.Loading ?
                                <ActivityIndicator color={'#fff'} size={32}/>
                                :
                                <Text style={[styles.Label, {color: '#fff'}]}>
                                    {Dict['t_main_confirm']}
                                </Text>
                        }
                    </TouchableOpacity>
                </ScrollView>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={3500}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        );
    }

    //---------------------Getting profile --------------------------------------------------------------------------------------
    _GetDetails() {
        Connect.SendPRequest(URLS.Link() + 'getprofile', {userId: this.props.id})
            .then(res => {
                if (res) {
                    this.setState({
                        CM: res.codeMeli ? true : false, //for edit codeMeli
                        Name: res.fName,
                        Family: res.lName,
                        CodeMelli: res.codeMeli,
                        FixedNumber: res.fixedNumber,
                        CompanyName: res.companyName,
                        Desc: res.desc,
                        ProfilePhoto: res.image,
                        Rank: res.rank,
                        CompanyAddress: res.address,
                        CityID: res.cityId,
                        City: res.CityName,
                        Province: res.ProvinceName,
                        ProvinceID: res.provinceId,
                        Phone: res.tell,
                    });
                    this._GetCity(res.cityId);
                }
            }).catch((e) => {
        });
    }

    //----------UpdateProfilePhoto----------------------------------------------------------------------------------------------------------
    _PhotoUpdate(Dict) {
        // console.warn('link is: '+URLS.Media())
        const PersonalPhotoName = (Math.ceil(new Date().getTime())).toString().replace('.', '') + '.jpg';
        ImagePicker.showImagePicker(Connect.ImagePickerOption(
            Dict['imagepicker_Title'],
            Dict['imagepicker_internal'],
            Dict['imagepicker_camera'],
            Dict['imagepicker_cancel'],
        ), (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                this.setState({
                        NewImageData: response.data,
                        UploadingPhoto: true,
                    },
                    () => {
                        RNFetchBlob.fetch('POST', URLS.Link() + 'upload', {
                            // RNFetchBlob.fetch('POST', URLS.Media(), {
                            Authorization: 'Bearer access-token',
                            otherHeader: 'foo',
                            'Content-Type': 'multipart/form-data',
                        }, [
                            {
                                name: 'file',
                                filename: PersonalPhotoName,
                                type: 'image/jpeg',
                                data: this.state.NewImageData,
                            },
                        ]).then((resp) => {
                            // console.warn(resp);
                            if (resp.respInfo.status == 200) {
                                this.setState({
                                    NewImageName: PersonalPhotoName,
                                    UploadingPhoto: false,
                                }, () => {
                                    this._UpdateProfile();
                                });
                            }else {
                                this.setState({UploadingPhoto: false});
                                this.dropdown.alertWithType('error', '', Dict['faild_update']);
                            }
                        }).catch((err) => {
                            this.setState({UploadingPhoto: false});
                            this.dropdown.alertWithType('error', '', Dict['faild_update']);
                        });
                    },
                );
            }
        });
    }

    //---------------------update profile --------------------------------------------------------------------------------------
    _UpdateProfile(Dict) {
        this.setState({Loading: true});
        // console.warn('image Nmae:' + this.state.NewImageName);
        Connect.SendPRequest(URLS.Link() + 'setprofile', {
            userId: parseInt(this.props.id),
            fName: this.state.Name,
            lName: this.state.Family,
            codeMeli: this.state.CodeMelli,
            fixedNumber: this.state.FixedNumber,
            companyName: this.state.CompanyName,
            desc: this.state.Desc,
            image: this.state.NewImageName ? this.state.NewImageName : this.state.ProfilePhoto,
            address: this.state.CompanyAddress,
            cityId: this.state.CityID,
            provinceId: this.state.ProvinceID,
            lat: '0',
            long: '0',
        }).then(res => {
            // console.warn('edit Profile: ');
            // console.warn(res);
            if (res) {
                this.props.DrawerUpdateFn();
                this.setState({Loading: false});
                this._GetDetails();
                this.dropdown.alertWithType('success', '', Dict['succsses_update']);
            } else {
                this.setState({Loading: false});
                this.dropdown.alertWithType('error', '', Dict['faild_update']);
            }
        }).catch((e) => {
            this.setState({Loading: false});
            this.dropdown.alertWithType('error', '', Dict['global_error']);
        });
    }

    //---------------------Get Province--------------------------------------------------------------------------------------
    _GetProvince() {
        Connect.SendPRequest(URLS.Link() + 'getprovince', {}).then(res => {
            // console.warn('name: '+res[1].name)
            AsyncStorage.getItem('language').then((lang) => {
                // console.warn('Lang: '+lang)
                let Ostan = [];
                let OstanID = [];
                for (let i = 0; i < res.length; i++) {
                    if (lang === 'En') {
                        Ostan.push(res[i].nameEn);
                        OstanID.push(res[i].id);
                    } else if (lang === 'Fa') {
                        Ostan.push(res[i].name);
                        OstanID.push(res[i].id);
                    } else if (lang === 'Ar') {
                        Ostan.push(res[i].nameAr);
                        OstanID.push(res[i].id);
                    } else if (lang === 'Ku') {
                        Ostan.push(res[i].nameKu);
                        OstanID.push(res[i].id);
                    }
                }
                this.setState({ProvinceList: Ostan, ProvinceIdList: OstanID});
            });
        }).catch((e) => {
            this.setState({Loading: false});
        });
    }

    //-------SelectProvince--------------------------------------------------------------------------------------------------
    _SelectProvince(Dict) {
        Picker.init({
            pickerFontColor: PickerFontColor,
            pickerToolBarBg: PickerToolBarBg,
            pickerConfirmBtnColor: PickerConfirmBtnColor,
            pickerCancelBtnColor: PickerCancelBtnColor,
            pickerTitleColor: PickerTitleColor,
            pickerBg: PickerBg,
            pickerTextEllipsisLen: 20,
            pickerConfirmBtnText: Dict['t_main_confirm'],
            pickerCancelBtnText: Dict['t_main_cancel'],
            pickerTitleText: '',
            // pickerData: data,
            pickerData: this.state.ProvinceList.length !== 0 ? this.state.ProvinceList : [''],
            onPickerConfirm: data => {
                // console.warn('Ostan ID: '+this.state.ProvinceIdList[this.state.ProvinceList.indexOf(data.toString())])
                this.setState({
                    Province: data.toString(),
                    ProvinceID: this.state.ProvinceIdList[this.state.ProvinceList.indexOf(data.toString())],
                }, () => this._GetCity(this.state.ProvinceIdList[this.state.ProvinceList.indexOf(data.toString())]));
            },
        });
        Picker.show();
    }

    //-------_GetCity--------------------------------------------------------------------------------------------------
    _GetCity(ID) {
        Connect.SendGRequest(URLS.Link() + `getcity?id=${parseInt(ID)}`)
            .then(res => {
                // console.warn(res)
                AsyncStorage.getItem('language').then((lang) => {
                    // console.warn('Lang: ' + lang)
                    let City = [];
                    let CityID = [];
                    for (let i = 0; i < res.length; i++) {
                        if (lang === 'En') {
                            City.push(res[i].nameEn);
                            CityID.push(res[i].id);
                        } else if (lang === 'Fa') {
                            City.push(res[i].name);
                            CityID.push(res[i].id);
                        } else if (lang === 'Ar') {
                            City.push(res[i].nameAr);
                            CityID.push(res[i].id);
                        } else if (lang === 'Ku') {
                            City.push(res[i].nameKu);
                            CityID.push(res[i].id);
                        } else {
                        }
                    }
                    this.setState({Loading: false, CityList: City, CityIdList: CityID});
                });
            }).catch((e) => {
            this.setState({Loading: false});
        });
    }

    //-------SelectCity--------------------------------------------------------------------------------------------------
    _SelectCity(Dict) {
        Picker.init({
            pickerFontColor: PickerFontColor,
            pickerToolBarBg: PickerToolBarBg,
            pickerConfirmBtnColor: PickerConfirmBtnColor,
            pickerCancelBtnColor: PickerCancelBtnColor,
            pickerTitleColor: PickerTitleColor,
            pickerBg: PickerBg,
            pickerTextEllipsisLen: 20,
            pickerConfirmBtnText: Dict['t_main_confirm'],
            pickerCancelBtnText: Dict['t_main_cancel'],
            pickerTitleText: '',
            // pickerData: data,
            pickerData: this.state.CityList.length !== 0 ? this.state.CityList : [''],
            onPickerConfirm: data => {
                // console.warn('city:'+ data.toString());
                this.setState({
                    City: data.toString(),
                    CityID: this.state.CityIdList[this.state.CityList.indexOf(data.toString())],
                }, () => null);
            },
        });
        Picker.show();
    }
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        drawer_update: state.drawer_update,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    // console.warn('dispatch: ',dispatch)
    return {
        getId: () => dispatch({type: 'getId'}),
        DrawerUpdateFn: () => dispatch({type: 'DrawerUpdateFn'}),

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
