import React from 'react';
import {
    FlatList,
    View,
    Text,
    AsyncStorage,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import {Connect} from '../../core/Connect';
import styles from '../../assets/css/main/TransactionList';
import Styles, {AppColorRed} from '../../assets/css/main/Styles';
import URLS from '../../core/URLS';
import Dic from '../../core/Dic';
import {connect} from 'react-redux';
import ListEmpty from '../../core/ListEmpty';
import Header from '../../core/Header';


class TransactionsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Factors: [],
            Empty: false,
        };
    }

    componentWillMount() {
        this.props.navigation.addListener('willFocus', payload => {
            this._GetFactors();
        });
    }

    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainView}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    style={styles.TransactionsFlatlist}
                    renderItem={(item) => <TransactionsList item={item} Dict={Dict}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Factors}
                    keyExtractor={(index, item) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
            </View>
        );
    }


    //---------------------Getting Orders --------------------------------------------------------------------------------------
    _GetFactors() {
        this.setState({Empty: true});
        Connect.SendPRequest(URLS.Link() + 'factors', {userId: parseInt(this.props.id)})
            .then(res => {
                // console.warn(res)
                if (res) {
                    this.setState({Factors: res, Empty: false});
                }
            }).catch((e) => {
            this.setState({Empty: false});
        });
    }
}

//---------------------redux-------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsList);