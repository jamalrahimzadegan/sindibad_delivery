import {Component} from 'react';
import {AsyncStorage, Dimensions, View, Text, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import Styles from '../../assets/css/main/Styles';
import styles from '../../assets/css/main/Unpaid';
import Header from '../../core/Header';
import URLS from "../../core/URLS";
import ListEmpty from "../../core/ListEmpty";
import RenderListOrder from "../flats/RenderListOrder";
import {Connect} from "../../core/Connect";
import Dic from '../../core/Dic';
import {connect} from 'react-redux';

 class Unpaid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Unpaids: [],
        };
    }

    componentWillMount() {
        this.props.navigation.addListener("willFocus", payload => {
            this._GetUnpaids();
        });
    }
    render() {
        let Dict = Dic[this.props.Language];
        return (
            <View style={styles.MainContianer}>
                <Header navigation={this.props.navigation}/>
                <FlatList
                    style={styles.UnpaidFlatlist}
                    renderItem={(item, index) => <RenderListOrder item={item} navigation={this.props.navigation}/>}
                    showsVerticalScrollIndicator={false}
                    data={this.state.Unpaids}
                    keyExtractor={(index) => index.toString()}
                    ListEmptyComponent={() => <ListEmpty
                        EmptyText={this.state.Empty ? Dict['loading'] : Dict['noitem']}
                        BgColor={'transparent'}/>}/>
                <DropdownAlert ref={ref => this.dropdown = ref} closeInterval={5000}
                               containerStyle={{backgroundColor: "red"}}
                               titleStyle={Styles.TTLErr} messageStyle={Styles.TTLErr}/>
            </View>
        )
    }
    //---------------------deliveryunpaid-------------------------------------------------------------------------------------
    _GetUnpaids() {
        Connect.SendPRequest(URLS.Link() + "deliveryunpaid", {userId: parseInt(this.props.id)})
            .then(res => {
                // console.log('unpaids')
                // console.log(res)
                if (res) {
                    this.setState({Unpaids: res});
                }
            });
    }
}
//-----------------------Redux--------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,

    };
}
function mapDispatchToProps(dispatch) {
    return {};
}
export default connect(mapStateToProps, mapDispatchToProps)(Unpaid);
