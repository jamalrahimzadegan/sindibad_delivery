import React from 'react';
import { View, Text,  Dimensions, StyleSheet,TouchableOpacity ,BackHandler} from 'react-native';
const { width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/AntDesign';
import NetInfo from "@react-native-community/netinfo";


export default class CheckConnect extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            isConnected:true,
        }
    }
    _handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({ isConnected });
        } else {
            this.setState({ isConnected });
        }
    };
    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectivityChange);
    }
    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectivityChange);
    }
    render(){
        if (!this.state.isConnected) {
            return <MiniOfflineSign />;
        }
        return null;
    }
}
function MiniOfflineSign() {
    return (
        <View style={styles.offlineContainer}>
            <View style={styles.MainDx}>
                <View style={{width:300,height:30}} />
                <View style={styles.Top1}>
                    <TouchableOpacity style={styles.topbtn} onPress={()=>BackHandler.exitApp()}>
                        <View style={styles.topxbtncirc}>
                            <Icon name={"close"} size={20} color={"gray"}/>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.Top2}>
                    <Text style={styles.offlineText}>Helper</Text>
                    <Text style={styles.offlineText3}>اینترنت قطع می باشد</Text>
                </View>

                <TouchableOpacity onPress={()=>BackHandler.exitApp()} style={styles.Btn}>
                    <Text style={styles.offlineText2}>خروج</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    topxbtncirc:{width:40,height:40,justifyContent:"center",alignItems:"center",borderWidth:1,borderColor:"#29a181",
        borderRadius:45,},
    topbtn:{width:55,height:55,justifyContent:"center",alignItems:"center",backgroundColor:"#fff",
    borderRadius:55,marginTop:-30},
    Top1:{width:300,height:40,justifyContent:"center",alignItems:"center",backgroundColor:"#ffffff",borderTopRightRadius:8,borderTopLeftRadius:8,},
    Top2:{width:300,height:125,alignItems:"center",backgroundColor:"#ffffff",},
    MainDx:{width:300,height:260,borderRadius:8,overflow:"hidden",},
    Btn:{width:300,height:65,justifyContent:"center",alignItems:"center",backgroundColor:"#29a181",},
    offlineContainer: {zIndex:1000,backgroundColor:'rgba(0,0,0,0.7)',height: Dimensions.get("window").
            height,justifyContent:'center',alignItems: 'center',width,position: 'absolute',},
    offlineText: {color: '#29a181',fontFamily:"kurdishFont",fontSize:17,},
    offlineText3: {color: 'gray',fontFamily:"kurdishFont",fontSize:17,marginTop:20},
    offlineText2: {color: '#fff',fontFamily:"kurdishFont",fontSize:17,}
});
