import {Component} from "react";
import React from "react";
import {AsyncStorage} from "react-native";
import firebase from "react-native-firebase";

export class Connect extends Component {
    constructor(props) {
        super(props);
    }

    //--------------PostRequest-------------------------------------------------------------------------------------------------
    static SendPRequest(action, Params = {}) {
        return fetch(action, {
            // return fetch(Link + action, {
            method: 'POST', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }, body: JSON.stringify(Params)
        }).then((response) => response.json())
    }

    //--------------GetRequest-------------------------------------------------------------------------------------------------
    static SendGRequest(action) {
        return fetch(action, {
            // return fetch(Link + action, {
            method: 'Get', headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
    }

    //--------------GetFcmToken-------------------------------------------------------------------------------------------------
    static GetFcmToken() {
        return firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    AsyncStorage.setItem('token', fcmToken);
                    // AsyncStorage.setItem('token','134');
                    // console.log('Firebase Token ==', fcmToken);
                    this.props.dispatch(fcmRegister(fcmToken));
                } else {
                    // user doesn't have a device token yet
                    // console.warn('User doesn\'t have a  token yet, Token = ', fcmToken);
                    firebase.messaging().requestPermission()
                }
            })
    }

//--------------Image picker option--------------------------------------------------

    static ImagePickerOption(a, b, c, d) {
        return {
            cropping:true,
            // title: 'jamal',
            title: a,
            chooseFromLibraryButtonTitle: b,
            takePhotoButtonTitle: c,
            cancelButtonTitle: d,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        }
    }

    //--------------FormatNumber-------------------------------------------------------------------------------------------------
    static FormatNumber(price) {
        return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

}
