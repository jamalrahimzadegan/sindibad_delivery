import styles from '../assets/css/Header';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/Entypo';
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    StatusBar,
} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import {AppColorRed} from '../assets/css/main/Styles';
import {connect} from 'react-redux';


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    render() {
        let Route = this.props.navigation.state.routeName;
        return (

            <View style={{backgroundColor: AppColorRed}}>
                <View style={styles.iosStatus}/>
                <StatusBar backgroundColor={AppColorRed} barStyle="light-content"/>
                <View
                    style={[styles.NavigationContainer, {flexDirection: this.props.f_direction}]}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name={'md-menu'} color={'#fff'} size={40}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.EachButton}
                                      onPress={() => this._GoHome()}>
                        <Icon2 name={'home'} color={'#fff'} size={35}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Icon1 name={'back'} color={'#fff'} size={30}/>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }

//-------Navigatie to Home Screen (Main)---------------------------------------------------------------------------
    _GoHome() {
        const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [
                NavigationActions.navigate({routeName: 'Main', params: {resetOrder: 1}}),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    };
}

//----------Redux---------------------------------------------------------------------------------------------
function mapStateToProps(state) {
    return {
        id: state.id,
        Language: state.Language,
        text_align: state.text_align,
        f_direction: state.f_direction,
    };
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);








