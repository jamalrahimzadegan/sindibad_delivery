export default class URLS {
    static SiteAddress(){
        return Host+"/";
    }
    static Link(){
        return Host+"/admin/mobile/";
    }
    static Media(){
        return Host+"/upload/";
        // return Host+"/admin/mobile/upload/";
    }
    static Slider(){
        return Host+"/upload/slider/";

    }
    static MediaSmall(){
        return Host+"/upload/smallthumbnail/";
    }
}
export const Host = 'http://www.gamingo.ir';
// export const Host = 'https://www.sindibadmarket.com';
