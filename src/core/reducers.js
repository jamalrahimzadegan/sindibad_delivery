import {createStore} from 'redux';
import {AsyncStorage} from 'react-native';


export const initialState = {
    id: null,
    Language: '',
    f_direction: '',
    text_align: '',
    drawer_update: 0,
};
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'setId':
            AsyncStorage.setItem('id', action.payload.id.toString());
            return {...state, id: action.payload.id};
        case 'setLang':
            return {
                ...state,
                Language: action.payload.Language,
                f_direction: action.payload.f_direction,
                text_align: action.payload.text_align,
            };
        case 'DrawerUpdateFn':
            return {...state, drawer_update:Math.random()};
        case 'PURGE':
            AsyncStorage.multiRemove(['phone', 'id']);
            return {...state, id: null};

    }
    return state;
};

export const store = createStore(reducer);


